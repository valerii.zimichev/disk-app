from django.apps import AppConfig


class MountpageConfig(AppConfig):
    name = 'mountpage'
