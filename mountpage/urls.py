from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', views.index),
    path('format/<disk>/', views.format),
    path('create/<disk>/', views.create),
    path('delete/<disk>/', views.delete),
    path('mount/<disk>/', views.mount),
    path('unmount/<disk>/', views.unmount),
]
