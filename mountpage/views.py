import requests
import urllib.parse
from django.shortcuts import render
from django.http import HttpResponse

import json
from subprocess import run, PIPE, Popen

from django.contrib.auth.decorators import login_required

def get_results(p,disk):
    output = p.stdout.decode("utf-8")
    exit = p.returncode
    return {'disk':disk, 'stdout':output, 'exit':exit}

@login_required
def index(request, **kwargs):
    '''
    MAIN PAGE
    '''
    command = ['lsblk -J']
    p = run(command, shell=True, stdout=PIPE)
    output = p.stdout.decode('utf-8').strip()
    data = json.loads(output)['blockdevices']
    f = open('create_params.txt', 'w')
    f.write('')
    f.close()

    return render(request, 'mountpage/index.html', {'data':data})

@login_required    
def format(request, **kwargs):
    '''
    FORMAT PAGE - default linux formatting
    '''    
    meta = locals()['kwargs']
    disk = meta['disk']

    command = [f'mkfs /dev/{disk}']
    p = run(command, shell=True, input=b'y', stdout=PIPE)
    return render(request, 'mountpage/format.html', {'data':get_results(p, disk)})

@login_required
def delete(request, **kwargs):
    '''
    DELETE PAGE - remove partition
    '''  
    meta = locals()['kwargs']
    disk = meta['disk']   
    command = [f'fdisk /dev/{disk}'] 
     
    if(request.method == "POST"):
        partition = request.POST['partition']
        p = run(command, shell=True, input=bytes(f'd\n{partition}\nw\n', 'utf-8'), stdout=PIPE)
        return render(request, 'mountpage/delete.html', {'data':get_results(p, disk)})
    if(request.method == "GET"): 
        p = run(command, shell=True, input=bytes(f'd\n', 'utf-8'), stdout=PIPE)
        return render(request, 'mountpage/delete.html', {'data':get_results(p, disk)})

@login_required        
def mount(request, **kwargs):
    '''
    MOUNT PAGE - create mountpoint
    '''  
    meta = locals()['kwargs']
    disk = meta['disk']   
         
    if(request.method == "POST"):
        mountpoint = request.POST['mountpoint']
        run(f'mkdir -p {mountpoint}', shell=True)
        command = [f'mount /dev/{disk} {mountpoint}'] 
        p = run(command, shell=True, stdout=PIPE)
        return render(request, 'mountpage/mount.html', {'data':get_results(p, disk)})
    if(request.method == "GET"): 
        data = {'disk':disk}
        return render(request, 'mountpage/mount.html', {'data':data})

@login_required        
def unmount(request, **kwargs):
    '''
    UnMOUNT PAGE - create mountpoint
    '''  
    meta = locals()['kwargs']
    disk = meta['disk']   
    command = [f'umount /dev/{disk}'] 
    p = run(command, shell=True, stdout=PIPE)
    return render(request, 'mountpage/unmount.html', {'data':get_results(p, disk)})

@login_required
def create(request, **kwargs):
    '''
    CREATE PAGE - create partition
    '''    
    meta = locals()['kwargs']
    disk = meta['disk'] 
    command = [f'fdisk /dev/{disk}'] 
    
    if(request.method == "POST"):
        inputs = request.POST['input']
        f = open('create_params.txt', 'a')
        f.write(inputs + '\n')
        f.close()
        f = open('create_params.txt', 'r')    
        inputs = f.read()    
        p = run(command, shell=True, input=bytes(f'n\n{inputs}w\n', 'utf-8'), stdout=PIPE)
    if(request.method == "GET"):
        p = run(command, shell=True, input=b'n\n', stdout=PIPE)  
    
    output = p.stdout.decode("utf-8")
    exit = p.returncode
        
    label = output.split(':')
    label = label[-3] if len(label)>3 else ''
    data = {'disk':disk, 'stdout':output, 'label':label, 'exit':exit}
    return render(request, 'mountpage/create.html', {'data':data})
